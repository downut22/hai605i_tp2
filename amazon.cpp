// test_couleur.cpp : Seuille une image couleur 

#include <stdio.h>
#include "image_ppm.h"
#include <math.h>       /* sqrt */

void seuillage(char* cNomImgLue,char* cNomImgEcrite, int seuil)
{  
  int nH, nW, nTaille;
  OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

 for (int i=0; i < nH; i++)
   for (int j=0; j < nW; j++)
     {
       if ( ImgIn[i*nW+j] > seuil) {
         ImgOut[i*nW+j]=255; 
       }else
       {
          ImgOut[i*nW+j]=0;
       }
     }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);
}

int surface(char* cNomImgLue)
{  
  int nH, nW, nTaille;
  OCTET *ImgIn;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);

  int s =0;
  for (int i=0; i < nH; i++)
   for (int j=0; j < nW; j++)
     {
       s += ImgIn[i*nW+j] / 255;
     }

   free(ImgIn); 
   return s;
}

void difference(char* img1,char * img2,char* cNomImgEcrite)
{
  int nH, nW, nTaille;
   OCTET *Img1,*Img2, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(img1, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(Img1, OCTET, nTaille);
   lire_image_pgm(img1, Img1, nH * nW);
   allocation_tableau(Img2, OCTET, nTaille);
   lire_image_pgm(img2, Img2, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
 for (int i=0; i < nH; i++){
   for (int j=0; j < nW; j++)
     {
       int p1 = Img1[i*nW+j];
       int p2 = Img2[i*nW+j];

        ImgOut[i*nW+j] = 
          (p1 == 255 && p2 == 255) ? 255 :
          (p1 == 0 && p2 == 0) ? 0 : 128;
     }
 }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(Img1); free(Img2);free(ImgOut);
}

bool voisinBlanc(OCTET* Img, int imgSize,int x, int y,bool* mask, int maskSize)
{
  int size2 = maskSize/2;
  for(int dx = -size2; dx < size2;dx++)
  {
    for(int dy = -size2; dy < size2;dy++)
    {
      int mx = size2+dx; int my = size2+dy;

      if(mask[(mx*maskSize)+my] == false){continue;}

      int xx = x + dx; int yy = y + dy;

      if(xx < 0 || xx >= imgSize || yy < 0 || yy >= imgSize){continue;}
    
      if(Img[(yy*imgSize)+xx] == 255){return true;}
    }
  }

  return false;
}

bool* genMask(int size)
{
  bool* m = new bool[size*size];

  int size2 = (size/2);
  for(int dx = -size2; dx < size2;dx++)
  {
    for(int dy = -size2; dy < size2;dy++)
    {
      int mx = size2+dx; int my = size2+dy;
      int d = pow(abs(dx)+1,2)+pow(abs(dy)+1,2)-1;
      m[(mx*size)+my] = d <= (size2*size2);
    }
  }

  return m;
}

void afficherMask(bool* mask, int size)
{
  int size2 = (size/2);
  for(int dx = -size2; dx < size2;dx++)
  {
    for(int dy = -size2; dy < size2;dy++)
    {
      int mx = size2+dx; int my = size2+dy;
      printf(" %c",mask[(mx*size)+my] ? '0' : ' ');
    }
    printf("\n");
  }
}

void dilatation(char* cNomImgLue,char* cNomImgEcrite, bool* mask,int distance)
{
  int nH, nW, nTaille;
   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);

 for (int i=0; i < nH; i++){
   for (int j=0; j < nW; j++)
     {
       /*int* voisins = new int[5]{
         ImgIn[i*nW+j],
         ImgIn[(i-1)*nW+j],
         ImgIn[(i+1)*nW+j],
         ImgIn[i*nW+(j-1)],
         ImgIn[i*nW+(j+1)]};

        int count = 0;
        for(int v = 0; v < 5; v++)
        {
            if(voisins[v] == 255){count++;}
        }

        ImgOut[i*nW+j] = count > 0 ? 255 : 0;*/
        ImgOut[i*nW+j] = voisinBlanc(ImgIn,nW,i,j,mask,distance) ? 255 : 0;
     }
 }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);
}


int main(int argc, char* argv[])
{
  char input1[250]; char input2[250]; char output[250];
  int seuil;

  if (argc != 5) 
     {
       printf("Usage: Image1.pgm Image2.pgm ImageOut.pgm Seuil \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",input1) ;
   sscanf (argv[2],"%s",input2) ;
   sscanf (argv[3],"%s",output);
   sscanf (argv[4],"%d",&seuil);

   char* img1 = "temp1.pgm";
   char* img2 = "temp2.pgm";
   seuillage(input1,img1,seuil);
   seuillage(input2,img2,seuil);

  bool* mask = genMask(20);
  afficherMask(mask,20);
   dilatation("temp1.pgm", "temp1.pgm",mask,20);
   dilatation("temp2.pgm", "temp2.pgm",mask,20);

   float surface1 = surface(img1);
   float surface2 = surface(img2);

   printf("Après 20-dilatation IMG1 possède %f pixels blancs\n",surface1);
   printf("IMG2 20-dilatation %f pixels blancs\n",surface2);

  float pixelSize = 0.25;
  surface1 *= pixelSize * pixelSize;
  surface2 *= pixelSize * pixelSize;

  printf("IMG1 a une surface non-animale de %f km2\n",surface1);
  printf("IMG2 a une surface non-animale de %f km2\n",surface2);

  float diff = surface2 - surface1;
  printf("%f km2 ont été perdus\n",diff);

  difference(img1,img2,output);

   return 1;
}
