// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include "image_ppm.h"

void erosion(int argc, char* cNomImgLue,char* cNomImgEcrite)
{
  int nH, nW, nTaille;

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   //   for (int i=0; i < nTaille; i++)
   // {
   //  if ( ImgIn[i] < S) ImgOut[i]=0; else ImgOut[i]=255;
   //  }


 for (int i=1; i < nH - 1; i++){
   for (int j=1; j < nW - 1; j++)
     {
       int pixel = ImgIn[i*nW+j];
       if(pixel == 255){ImgOut[i*nW+j] = 255;continue;}

       int* voisins = new int[4]{
         ImgIn[(i-1)*nW+j],
         ImgIn[(i+1)*nW+j],
         ImgIn[i*nW+(j-1)],
         ImgIn[i*nW+(j+1)]};

        int count = 0;
        for(int v = 0; v < 4; v++)
        {
            if(voisins[v] == 255){count++;}
        }

        ImgOut[i*nW+j] = count > 0 ? 255 : ImgIn[i*nW+j]; 
     }
 }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);
}


void dilatation(int argc, char* cNomImgLue,char* cNomImgEcrite)
{
  int nH, nW, nTaille;

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   //   for (int i=0; i < nTaille; i++)
   // {
   //  if ( ImgIn[i] < S) ImgOut[i]=0; else ImgOut[i]=255;
   //  }


 for (int i=1; i < nH - 1; i++){
   for (int j=1; j < nW - 1; j++)
     {
       int pixel = ImgIn[i*nW+j];
       if(pixel == 0){ImgOut[i*nW+j] = 0;continue;}

       int* voisins = new int[4]{
         ImgIn[(i-1)*nW+j],
         ImgIn[(i+1)*nW+j],
         ImgIn[i*nW+(j-1)],
         ImgIn[i*nW+(j+1)]};

        int count = 0;
        for(int v = 0; v < 4; v++)
        {
            if(voisins[v] == 0){count++;}
        }

        ImgOut[i*nW+j] = count > 0 ? 0 : ImgIn[i*nW+j]; 
     }
 }

   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);
}

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];

  if (argc != 3)
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

  dilatation(argc,cNomImgLue,cNomImgEcrite);
  for(int i = 0; i < 2;i++){
    dilatation(argc,cNomImgEcrite,cNomImgEcrite);
  }
  for(int i = 0; i < 6;i++){
    erosion(argc,cNomImgEcrite,cNomImgEcrite);
  }for(int i = 0; i < 3;i++){
    dilatation(argc,cNomImgEcrite,cNomImgEcrite);
  }
  return 1;
}
